import tensorflow as tf
import deep_fully_connected as dfc
import cifar_helpers as ch

TRAINING_RECORDS = ['../cifar_test.tfrecords']
SAVE_FILE = './save/fc_model_params.ckpt'

NUM_EPOCHS = 1
BATCH_SIZE = 10000 # The entire test set


def main():
    print('CIFAR10 Fully Connected Test Accuracy')

    image_batch, label_batch = ch.cifar_input_pipeline(TRAINING_RECORDS, num_epochs=NUM_EPOCHS, batch_size=BATCH_SIZE)

    print('Construct feedforward graph...')
    nn_output, is_training_t= dfc.fully_connected_feedforward_with_batch_normalization(image_batch)

    num_correct_t, num_samples_t = ch.add_test_accuracy_metrics(nn_output, label_batch)

    ch.run_test_accuracy(num_correct_t, num_samples_t, is_training_t, SAVE_FILE)

if __name__ == '__main__':
    main()