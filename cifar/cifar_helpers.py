import tensorflow as tf
from datetime import datetime
import os

CIFAR_CATEGORIES = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']


def cifar_input_pipeline(file_list, num_epochs, batch_size):
    print('Setup input pipeline...')
    filename_queue = tf.train.string_input_producer(file_list, num_epochs=num_epochs)
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    features = tf.parse_single_example(
        serialized_example,
        features={
            'label': tf.FixedLenFeature([], tf.int64),
            'image': tf.FixedLenFeature([], tf.string)
        })
    label = tf.one_hot(features['label'], 10)
    image = tf.decode_raw(features['image'], tf.uint8)

    with tf.name_scope('input_reshape'):
        image = tf.reshape(image, [32, 32, 3])

        image = tf.image.flip_left_right(image)
    [image_batch, label_batch] = tf.train.batch([image, label], batch_size=batch_size)

    return image_batch, label_batch


def cost_function_and_accuracy(nn_output, label_batch):
    print('Setup loss...')
    with tf.name_scope('loss'):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=label_batch,
                                                                logits=nn_output)
    cross_entropy = tf.reduce_mean(cross_entropy)
    tf.summary.scalar('cross_entropy', cross_entropy)

    with tf.name_scope('adam_optimizer'):
        train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

    with tf.name_scope('accuracy'):
        correct_prediction = tf.equal(tf.argmax(nn_output, 1), tf.argmax(label_batch, 1))
        correct_prediction = tf.cast(correct_prediction, tf.float32)
    accuracy = tf.reduce_mean(correct_prediction)
    tf.summary.scalar('accuracy', accuracy)

    return train_step, accuracy


def add_test_accuracy_metrics(nn_output, label_batch):
    print('Adding accuracy metrics...')
    with tf.name_scope('accuracy_metrics'):
        num_correct = tf.equal(tf.argmax(nn_output, 1), tf.argmax(label_batch, 1))
        num_correct = tf.reduce_sum(tf.cast(num_correct, tf.int32))
        num_samples = tf.shape(label_batch)[0]

    return num_correct, num_samples


def create_writer_and_output_graph():
    graph_location = './graph/run-{}'.format(datetime.utcnow().strftime('%Y%m%d%H%M%S'))
    print('Saving graph to: %s' % graph_location)
    return tf.summary.FileWriter(graph_location)


def create_saver_and_attempt_restore(session, save_file_name):
    saver = tf.train.Saver()

    if os.path.isfile(save_file_name + '.index'):
        print('Restoring session...')
        saver.restore(session, save_file_name)

    else:
        print('No save file found.')

    return saver


# Outputs the category name (string) corresponding to the one hot sample label.
def cifar_one_hot_to_label(one_hot):
    index = -1
    for i in range(len(one_hot)):
        if one_hot[i]:
            index = i
            break

    return CIFAR_CATEGORIES[index]


def run_training(train_step, accuracy, is_training, save_file):
    train_writer = create_writer_and_output_graph()

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        train_writer.add_graph(sess.graph)

        saver = create_saver_and_attempt_restore(sess, save_file)

        coord = tf.train.Coordinator()
        tf.train.start_queue_runners(sess=sess, coord=coord)

        merged_summary = tf.summary.merge_all()

        i = 0
        try:
            while not coord.should_stop():
                if i % 100 == 0:
                    [s, train_accuracy] = sess.run([merged_summary, accuracy], feed_dict={is_training: False})
                    train_writer.add_summary(s, i)
                    print('step %d, training accuracy %g' % (i, train_accuracy))
                else:
                    sess.run([train_step], feed_dict={is_training: True})

                if i % 2000 == 0:
                    saver.save(sess, save_file)

                i += 1
        except tf.errors.OutOfRangeError:
            print('Done training.')
        finally:
            coord.request_stop()

        coord.join()


def run_test_accuracy(num_correct_t, batch_size_t, is_training_t, save_file):
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        create_saver_and_attempt_restore(sess, save_file)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess, coord=coord)

        total_correct = 0
        total_samples = 0
        i = 0

        print('Running training accuracy...')
        try:
            while not coord.should_stop():
                [num_correct, num_samples] = sess.run([num_correct_t, batch_size_t], feed_dict={is_training_t: False})
                total_correct += num_correct
                total_samples += num_samples

                # if i % 100 == 0:
                print('Test metrics -> {} samples out of {} are correct.'.format(num_correct, num_samples))
                # i += 1

        except tf.errors.OutOfRangeError:
            print('Done training.')
        finally:
            coord.request_stop()

        print('\nFinal results...')
        print('A total of {} out of {} samples are correct.'.format(total_correct, total_samples))
        print('Test accuracy: {}'.format(total_correct/total_samples))
        coord.join(threads)
