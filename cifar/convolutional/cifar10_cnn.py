import tensorflow as tf
import numpy as np
from PIL import Image
from tensorflow.contrib.layers import batch_norm
from tensorflow.contrib.layers import fully_connected
from datetime import datetime

NUM_NODES_HIDDEN_LAYER = 5000

NUM_CONV_LAYERS = 10


def weight_variable(shape):
  """weight_variable generates a weight variable of a given shape."""
  initial = tf.truncated_normal(shape, stddev=0.05)
  return tf.Variable(initial, name='W')


def bias_variable(shape):
  """bias_variable generates a bias variable of a given shape."""
  # initial = tf.constant(0.1, shape=shape)
  initial = tf.random_normal(shape, stddev=0.05)
  return tf.Variable(initial, name='b')


# def fully_connected_layer(input, input_dimension, nodes):
#     with tf.name_scope('fully_connected'):
#         weight = weight_variable([input_dimension, nodes])
#         bias = bias_variable([nodes])
#         return tf.matmul(input, weight) + bias


def conv2d(x, W):
  """conv2d returns a 2d convolution layer with full stride."""
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
  """max_pool_2x2 downsamples a feature map by 2X."""
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')


def convolution(input, conv_shape):
    with tf.name_scope('conv'):
        W_conv = weight_variable(conv_shape)
        b_conv = bias_variable([conv_shape[3]])
        return tf.nn.relu(conv2d(input, W_conv) + b_conv)


def convolution_and_pool(input, conv_shape):
    h_conv = convolution(input, conv_shape)

    # Second pooling layer.
    with tf.name_scope('pool'):
        return max_pool_2x2(h_conv)


def cnn_with_fully_connected_layers(image):
    is_training = tf.placeholder(tf.bool, shape=(), name='is_training')
    bn_params = {
        'is_training': is_training,
        'decay': 0.99,
        'updates_collections': None
    }
    tf.summary.image('input', image)

    with tf.name_scope('typecast_input'):
        input = tf.cast(image, tf.float32)

    conv_output = convolution_and_pool(input, [5, 5, 3, 128])
    for i in range(NUM_CONV_LAYERS):
        conv_output = convolution(conv_output, [5, 5, 128, 128])

    print('Conv output shape = {}'.format(conv_output.get_shape()))
    conv2 = convolution_and_pool(conv_output, [5, 5, 128, 128])

    conv2_flattened = tf.reshape(conv2, [-1, 8*8*128])
    hidden1 = fully_connected(conv2_flattened, NUM_NODES_HIDDEN_LAYER, scope="fc1", normalizer_fn=batch_norm,
                              normalizer_params=bn_params)

    hidden2 = fully_connected(hidden1, NUM_NODES_HIDDEN_LAYER, scope="fc2", normalizer_fn=batch_norm,
                              normalizer_params=bn_params)
    output = fully_connected(hidden2, 10, scope="output", activation_fn=None, normalizer_fn=batch_norm,
                              normalizer_params=bn_params)

    return output, is_training


def all_cnn(image):
    is_training = tf.placeholder(tf.bool, shape=(), name='is_training')
    tf.summary.image('input', image)
    with tf.name_scope('typecast_input'):
        input = tf.cast(image, tf.float32)

    activation = tf.nn.elu
    x = input

    x = tf.layers.dropout(x, rate=0.2, training=is_training)
    x = tf.layers.conv2d(x, 96, [3, 3], activation=activation, padding='same')
    x = tf.layers.conv2d(x, 96, [3, 3], activation=activation, padding='same')
    x = tf.layers.conv2d(x, 96, [3, 3], activation=activation, padding='same', strides=(2, 2))

    x = tf.layers.dropout(x, rate=0.5, training=is_training)
    x = tf.layers.conv2d(x, 192, [3, 3], activation=activation, padding='same')
    x = tf.layers.conv2d(x, 192, [3, 3], activation=activation, padding='same')
    x = tf.layers.conv2d(x, 192, [3, 3], activation=activation, padding='same', strides=(2, 2))

    x = tf.layers.dropout(x, rate=0.5, training=is_training)
    x = tf.layers.conv2d(x, 192, [3, 3], activation=activation, padding='valid')
    x = tf.layers.conv2d(x, 192, [1, 1], activation=activation, padding='valid')
    x = tf.layers.conv2d(x, 10, [1, 1], activation=activation, padding='valid')

    x = tf.layers.average_pooling2d(x, [6, 6], 1, padding='valid')
    output = tf.reshape(x, [-1, 10])

    return output, is_training