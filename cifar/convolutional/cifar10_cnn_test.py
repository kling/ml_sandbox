import cifar_helpers as ch
import cifar10_cnn as cnn

NUM_EPOCHS = 1
BATCH_SIZE = 50 # The entire test set

SAVE_FILE = './convolutional/save/cnn_model_params.ckpt'
TRAINING_RECORDS = ['cifar_test.tfrecords']


def main():
    print('CIFAR10 Convolutional Neural Net Training')

    image_batch, label_batch = ch.cifar_input_pipeline(TRAINING_RECORDS, num_epochs=NUM_EPOCHS, batch_size=BATCH_SIZE)

    print('Construct feedforward graph...')
    nn_output, is_training_t = cnn.all_cnn(image_batch)

    num_correct_t, num_samples_t = ch.add_test_accuracy_metrics(nn_output, label_batch)

    ch.run_test_accuracy(num_correct_t, num_samples_t, is_training_t, SAVE_FILE)

if __name__ == '__main__':
    main()