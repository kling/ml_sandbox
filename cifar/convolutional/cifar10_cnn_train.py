import tensorflow as tf
import cifar_helpers as ch
import cifar10_cnn as cnn
from datetime import datetime

NUM_EPOCHS = 50
BATCH_SIZE = 128 # The entire test set

SAVE_FILE = './save/cnn_model_params.ckpt'

TRAINING_RECORDS = ['../cifar_training_set_1.tfrecords',
                    '../cifar_training_set_2.tfrecords',
                    '../cifar_training_set_3.tfrecords',
                    '../cifar_training_set_4.tfrecords',
                    '../cifar_training_set_5.tfrecords']


def main():
    print('CIFAR10 Convolutional Neural Net Training')

    image_batch, label_batch = ch.cifar_input_pipeline(TRAINING_RECORDS, num_epochs=NUM_EPOCHS, batch_size=BATCH_SIZE)

    print('Construct feedforward graph...')
    # nn_output, is_training = cnn.cnn_with_fully_connected_layers(image_batch)
    nn_output, is_training = cnn.all_cnn(image_batch)

    train_step, accuracy = ch.cost_function_and_accuracy(nn_output, label_batch)

    ch.run_training(train_step, accuracy, is_training, SAVE_FILE)


if __name__ == '__main__':
    main()