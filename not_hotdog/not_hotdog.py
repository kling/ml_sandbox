
import os
import json
import random
import tensorflow as tf
import tempfile
import matplotlib.pyplot as plt


# For debugging
DISABLE_CACHE = False

CACHED_DATA_FILE = 'not_hot_dog_cache.json'
COCO_HOT_DOG_CATEGORY_ID = 58;

train_instances_file = open('D:/Code/coco_dataset/annotations_trainval2017/annotations/instances_train2017.json')
validation_instances_file = open('D:/Code/coco_dataset/annotations_trainval2017/annotations/instances_val2017.json')

train_image_directory = 'D:/Code/coco_dataset/train2017/train2017/'

# Returns a dictionary of sor
def create_image_dictionary(coco_instances):
    images = {}
    for image in coco_instances['images']:
        images.update({image['id'] : image})

    return images


# Returns a set of image IDs for all images with hot dogs.
def find_all_hot_dog_images(coco_instances):
    hot_dog_image_ids = []
    for annotation in coco_instances['annotations']:
        if annotation['category_id'] == COCO_HOT_DOG_CATEGORY_ID:
            hot_dog_image_ids.append(annotation['image_id'])

    return set(hot_dog_image_ids)


# Returns a dictionary. Keys are image IDs in the training set and the value is whether the image has a hot dog.
# {image_id : is_hot_dog}
def create_training_set(hot_dog_image_ids, num_additional_images, images):
    training_images_and_label = {}
    remaining_images = images.copy()
    for image_id in hot_dog_image_ids:
        remaining_images.pop(image_id)
        training_images_and_label.update({image_id : True})

    remaining_image_ids = list(remaining_images.keys())
    num_images_to_add = min(len(remaining_image_ids), num_additional_images)
    for i in range(num_images_to_add):
        index = random.randint(0, len(remaining_image_ids)-1)
        image_id = remaining_image_ids[index]
        training_images_and_label.update({image_id : False})
        del remaining_image_ids[index]

    training_set = {}
    for image_id in training_images_and_label:
        training_sample = {'file_name': images[image_id]['file_name'], 'is_hot_dog': training_images_and_label[image_id]}
        training_set.update({image_id: training_sample})

    return training_set


# Converts a list of image_ids to a list of image_file_names.
def convert_image_ids_to_file_names(image_ids, images):
    file_names = []
    for image_id in image_ids:
        file_names.append(images[image_id]['file_name'])

    return file_names


def deepnn(x):
  """deepnn builds the graph for a deep net for classifying digits.

  Args:
    x: an input tensor with the dimensions (N_examples, 784), where 784 is the
    number of pixels in a standard MNIST image.

  Returns:
    A tuple (y, keep_prob). y is a tensor of shape (N_examples, 10), with values
    equal to the logits of classifying the digit into one of 10 classes (the
    digits 0-9). keep_prob is a scalar placeholder for the probability of
    dropout.
  """
  # Reshape to use within a convolutional neural net.
  # Last dimension is for "features" - there is only one here, since images are
  # grayscale -- it would be 3 for an RGB image, 4 for RGBA, etc.
  with tf.name_scope('reshape'):
    x_image = tf.reshape(x, [-1, 320, 240, 3])

  # First convolutional layer - maps each RGB channel to 32 feature maps
  with tf.name_scope('conv1'):
    W_conv1 = weight_variable([5, 5, 3, 32])
    b_conv1 = bias_variable([32])
    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)

  # Pooling layer - downsamples by 4X.
  with tf.name_scope('pool1'):
    h_pool1 = max_pool_4x4(h_conv1)

  # Second convolutional layer -- maps 32 feature maps to 64.
  with tf.name_scope('conv2'):
    W_conv2 = weight_variable([5, 5, 32, 64])
    b_conv2 = bias_variable([64])
    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)

  # Second pooling layer.
  with tf.name_scope('pool2'):
    h_pool2 = max_pool_4x4(h_conv2)

  # Fully connected layer 1 -- after 2 round of downsampling, our 640x480 image
  # is down to 40x30x64 feature maps -- maps this to 1024 features.
  with tf.name_scope('fc1'):
    W_fc1 = weight_variable([20*15*64, 1024])
    b_fc1 = bias_variable([1024])

    h_pool2_flat = tf.reshape(h_pool2, [-1, 20*15*64])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

  # Dropout - controls the complexity of the model, prevents co-adaptation of
  # features.
  with tf.name_scope('dropout'):
    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

  # Map the 1024 features to 1 output
  with tf.name_scope('fc2'):
    W_fc2 = weight_variable([1024, 2])
    b_fc2 = bias_variable([2])

    print('h_fc1:')
    print(h_fc1.get_shape())
    print('W_fc2:')
    print(W_fc2.get_shape())
    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
  return y_conv, keep_prob


def conv2d(x, W):
  """conv2d returns a 2d convolution layer with full stride."""
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_4x4(x):
  return tf.nn.max_pool(x, ksize=[1, 4, 4, 1],
                        strides=[1, 4, 4, 1], padding='SAME')


def weight_variable(shape):
  """weight_variable generates a weight variable of a given shape."""
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)


def bias_variable(shape):
  """bias_variable generates a bias variable of a given shape."""
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)



_batch_size = 0
_next_image = 0
_training_set = {}
_training_queue = []

def init_training_set(training_set, batch_size):
    global _training_set
    global _training_queue
    global _next_image
    global _batch_size

    _training_set = training_set
    _training_queue = list(training_set.keys())
    _training_queue = [train_image_directory+_training_set[x]['file_name'] for x in _training_queue]
    _next_image = 0
    _batch_size = batch_size


def run_nn_training():
    labels_queue = [_training_set[x]['is_hot_dog'] for x in _training_set]
    one_hot_labels_queue = []
    for x in labels_queue:
        if x:
            one_hot_labels_queue.append([0, 1])
        else:
            one_hot_labels_queue.append([1, 0])

    one_hot_labels_queue = tf.convert_to_tensor(one_hot_labels_queue, dtype=tf.int32)
    filename_queue = tf.convert_to_tensor(_training_queue, dtype=tf.string)

    training_queue = tf.train.slice_input_producer([filename_queue, one_hot_labels_queue], shuffle=True)

    image_file_contents = tf.read_file(training_queue[0])
    image = tf.image.decode_jpeg(image_file_contents, channels=3)
    resized_image = tf.image.resize_images(image, [320, 240])
    labels = training_queue[1]
    [image_batch, label_batch] = tf.train.batch([resized_image, labels], batch_size=10)

    # Build the graph for the deep net
    y_conv, keep_prob = deepnn(image_batch)

    with tf.name_scope('loss'):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=label_batch,
                                                                logits=y_conv)
    cross_entropy = tf.reduce_mean(cross_entropy)

    with tf.name_scope('adam_optimizer'):
        train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

    with tf.name_scope('accuracy'):
        correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(label_batch, 1))
        correct_prediction = tf.cast(correct_prediction, tf.float32)
    accuracy = tf.reduce_mean(correct_prediction)

    graph_location = './graph'
    print('Saving graph to: %s' % graph_location)
    train_writer = tf.summary.FileWriter(graph_location)

    with tf.Session() as sess:
        train_writer.add_graph(sess.graph)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        sess.run(tf.global_variables_initializer())
        for i in range(20000):
            if i % 100 == 0:
                train_accuracy = accuracy.eval(feed_dict={keep_prob: 1.0})
                print('step %d, training accuracy %g' % (i, train_accuracy))
            train_step.run(feed_dict={keep_prob: 0.5})

            # print('test accuracy %g' % accuracy.eval(feed_dict={
            #     x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))

        coord.request_stop()
        coord.join(threads)


def prepare_data_set(coco_instances_json_file, num_additional_images):
    coco_instances = json.load(coco_instances_json_file)
    images = create_image_dictionary(coco_instances)
    hot_dog_image_ids = find_all_hot_dog_images(coco_instances)
    return create_training_set(hot_dog_image_ids, num_additional_images, images)


def main():
    if not os.path.isfile(CACHED_DATA_FILE) or DISABLE_CACHE:
        print('No cache found, prepping training data.')
        training_set = prepare_data_set(train_instances_file, 9000)

        print('Prepping validation data.')
        validation_set = prepare_data_set(validation_instances_file, 3000)

        print('Caching training and validation data.')
        with open(CACHED_DATA_FILE, 'w') as outfile:
            cache_data = {'training_set': training_set, 'validation_set': validation_set}
            json.dump(cache_data, outfile)
    else:
        print('Cache found.')
        cached_data = json.load(open(CACHED_DATA_FILE))
        training_set = cached_data['training_set']
        validation_set = cached_data['validation_set']

    init_training_set(training_set, 50)

    print('Starting ML stuff.')
    run_nn_training()



if __name__ == '__main__':
    main()
